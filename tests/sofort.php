<?php

session_start();

if (!isset($_SESSION['apikey']) || !isset($_SESSION['activeaccount']))
{
    die('Please set api key and account on home page.');
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Example Merchant Page</title>
    <link rel="stylesheet" href="../resources/css/bootstrap.min.css">
</head>

<body>
<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Example Merchant</a>
        </div>
    </div>
</nav>

<div class="container">

    <h3>Welcome to the checkout page</h3>

    <form id="MyForm" action="/checkout/sofort.php" method="post">
        <div class="form-group">
            <label for="amount" class="col-lg-2 control-label">Amount</label>
            <div class="col-lg-10">
                <input type="text" name="amount" id="amount" class="form-control">
            </div>
        </div>
    </form>
</div>
</body>

<script src="https://sandbox.spryngpayments.com/cdn/jsclient.js"></script>

<script>
    var options = {
      payment_products: ['sofort']
    };

    jsclient.injectForm(document.getElementById("MyForm"), options)
</script>
</html>
