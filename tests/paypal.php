<?php

session_start();

if (!isset($_SESSION['apikey']) || !isset($_SESSION['activeaccount']))
{
    die('Please set api key and account on home page.');
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Example Merchant Page</title>
    <link rel="stylesheet" href="../resources/css/bootstrap.min.css">
</head>

<body>
<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Example Merchant</a>
        </div>
    </div>
</nav>

<div class="container">

    <h3>Welcome to the checkout page</h3>

    <form id="MyForm" action="/checkout/paypal.php" method="post">
        <div class="form-group">
            <label for="amount" class="col-lg-2 control-label">Amount</label>
            <div class="col-lg-10">
                <input type="text" name="amount" id="amount" class="form-control">
            </div>
        </div>

        <h3>Customer info</h3>

        <div class="form-group">
            <label for="customerId" class="col-lg-2 control-label">ID (should use this one for now)</label>
            <div class="col-lg-10">
                <input type="text" name="customerId" id="customerId" value="5800ace65f7f1529ab53e23f" class="form-control">
            </div>
        </div>
    </form>

    <h2>Paypal Test account for Spryng Paypal account</h2>
    <label for="username">Username/email</label>
    <p>contact-buyer@roemerbakker.com</p>

    <label for="password">Password</label>
    <p>*$6=^64376.7[&$486$?</p>
</div>
</body>

<script src="../resources/js/jsclient-2.2.2.js"></script>

<script>
    var options = {
        submit_title: "Start Paypal",
        payment_products: ['paypal']
    };

    jsclient.injectForm(document.getElementById("MyForm"), options)
</script>
</html>