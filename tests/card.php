<?php

session_start();

if (!isset($_SESSION['apikey']) || !isset($_SESSION['activeaccount']) || !isset($_SESSION['activeorganisation']))
{
    die('Please set api key, account and organisation on home page.');
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Example Merchant Page</title>
    <link rel="stylesheet" href="../resources/css/bootstrap.min.css">
</head>

<body>
<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Example Merchant</a>
        </div>
    </div>
</nav>

<div class="container">

    <h3>Welcome to the checkout page</h3>

    <form id="MyForm" action="/checkout/card.php" method="POST">
        <div class="form-group">
            <label for="amount" class="col-lg-2 control-label">Amount</label>
            <div class="col-lg-10">
                <input type="text" name="amount" id="amount" class="form-control">
            </div>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
        </div>
    </form>
</div>
</body>

<script src="../resources/js/jsclient_prod.min.js"></script>

<script>
    var options = {
        cvv_placeholder_3: "123",
        card_number_placeholder: "0000 0000 0000 0000",
        submit_title: "Start Credit Card Payment",
        payment_products: ['card'],
        cardstore_url: "https://api.spryngpayments.com/v1/card/",
        organisation_id: "<?php echo $_SESSION['activeorganisation']; ?>",
        account_id: "<?php echo $_SESSION['activeaccount']; ?>"
    };

    jsclient.injectForm(document.getElementById("MyForm"), options)
</script>
</html>