<?php

session_start();

if (!isset($_SESSION['apikey']) || !isset($_SESSION['activeaccount']))
{
    die('Please set api key and account on home page.');
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Example Merchant Page</title>
    <link rel="stylesheet" href="../resources/css/bootstrap.min.css">
</head>

<body>
<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Example Merchant</a>
        </div>
    </div>
</nav>

<div class="container">

    <h3>Welcome to the checkout page</h3>

    <form id="MyForm" action="/checkout/klarna.php" method="post">

        <div class="form-group">
            <label for="amount" class="col-lg-2 control-label">Amount</label>
            <div class="col-lg-10">
                <input type="text" name="amount" id="amount" value="10000" class="form-control">
            </div>
        </div>

        <div class="form-group">
            <label for="country_code" class="col-lg-2 control-label">Country</label>
            <div class="col-lg-10">
                <select name="country_code" id="country_code" class="form-control">
                    <option value="AT">Austria</option>
                    <option value="DE">Germany</option>
                    <option value="DK">Denmark</option>
                    <option value="FI">Finland</option>
                    <option selected="" value="NL">The Netherlands</option>
                    <option value="NO">Norway</option>
                    <option value="SE">Sweden</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="gender" class="col-lg-2 control-label">Gender</label>
            <div class="col-lg-10">
                <select name="gender" id="gender" class="form-control">
                    <option selected="" value="male">Male</option>
                    <option value="female">Female</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="title" class="col-lg-2 control-label">Title</label>
            <div class="col-lg-10">
                <select name="title" id="title" class="form-control">
                    <option selected="" value="mr">Mr.</option>
                    <option value="ms">Ms.</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="first_name" class="col-lg-2 control-label">First Name</label>
            <div class="col-lg-10">
                <input type="text" name="first_name" id="first_name" value="Roemer" class="form-control">
            </div>
        </div>

        <div class="form-group">
            <label for="last_name" class="col-lg-2 control-label">Last Name</label>
            <div class="col-lg-10">
                <input type="text" name="last_name" id="last_name" value="Bakker" class="form-control">
            </div>
        </div>

        <div class="form-group">
            <label for="email_address" class="col-lg-2 control-label">Email</label>
            <div class="col-lg-10">
                <input type="email" name="email_address" id="email_address" value="roemer@spryngpayments.com" class="form-control">
            </div>
        </div>

        <div class="form-group">
            <label for="phone_number" class="col-lg-2 control-label">Phone number</label>
            <div class="col-lg-10">
                <input type="text" name="phone_number" id="phone_number" value="+31612345678" class="form-control">
            </div>
        </div>

        <div class="form-group">
            <label for="city" class="col-lg-2 control-label">City</label>
            <div class="col-lg-10">
                <input type="text" name="city" id="city" value="Amsterdam" class="form-control">
            </div>
        </div>

        <div class="form-group">
            <label for="street_address" class="col-lg-2 control-label">Street address</label>
            <div class="col-lg-10">
                <input type="text" name="street_address" id="street_address" value="Stationsplein 51" class="form-control">
            </div>
        </div>

        <div class="form-group">
            <label for="postal_code" class="col-lg-2 control-label">Postcode</label>
            <div class="col-lg-10">
                <input type="text" name="postal_code" id="postal_code" value="1012 AB" class="form-control">
            </div>
        </div>

        <div class="form-group">
            <label for="date_of_birth" class="col-lg-2 control-label">Date of birth</label>
            <div class="col-lg-10">
                <input type="date" name="date_of_birth" id="date_of_birth" value="1997-02-28" class="form-control">
            </div>
        </div>

    </form>
    <br>
    <br>
    <br>
</div>
</body>
<script src="../resources/js/jquery.min.js"></script>
<script src="../resources/js/jsclient-2.2.2.js"></script>
<script>
    $(document).ready(function()
    {
        var accountId = "<?php echo $_SESSION['activeaccount']; ?>";
        var amount = 10000;

        $('#amount').on('change', function(e)
        {
            if (isNaN($(this).val()))
            {
                amount = 0;
            }
            else
            {
                amount = $(this).val();
            }
        });


        var options =
        {
            account_id:         accountId,
            amount:             amount,
            country_code:       $('#country_code').val(),
            eid:                6893,
            payment_products:   ['klarna']
        };

        jsclient.injectForm(document.getElementById("MyForm"), options)
    });
</script>
</html>