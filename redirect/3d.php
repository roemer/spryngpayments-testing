<?php

session_start();

if (!isset($_SESSION['apikey']) || !isset($_SESSION['activeaccount']))
{
    die('Please set api key and account on home page.');
}

if (!isset($_POST['PaRes']) || !isset($_POST['MD']))
{
    var_dump($_POST);
    die('Something went wrong...');
}

$post = array(
    "account"   => $_SESSION['activeaccount'],
    "pares"     => $_POST['PaRes']
);

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, "https://sandbox.spryngpayments.com/v1/3d/authorization");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    "X-APIKEY: ".$_SESSION['apikey']
));
$rawResponse = curl_exec($ch);
$jsonResponse = json_decode($rawResponse);

var_dump($jsonResponse);