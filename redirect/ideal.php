<?php

session_start();

if (!isset($_SESSION['apikey']) || !isset($_SESSION['activeaccount']))
{
    die('Please set api key and account on home page.');
}

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, "https://api.spryngpayments.com/v1/transaction/".$_GET['transaction_id']);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    "X-APIKEY: ".$_SESSION['apikey']
));

$rawResponse = curl_exec($ch);
$jsonResponse = json_decode($rawResponse);

if ($jsonResponse->status == 'SETTLEMENT_COMPLETED')
{
    echo "<p style='color:green;'>iDeal payment successful! <a href='/'>Click here to return to the homepage.</a></p>";
}
else
{
    echo "<p style='color:red;'>Error with payment! <a href='/'>Click here to return to the homepage.</a></p>";
}

echo "<h2>Full response: </h2>";

var_dump($jsonResponse);