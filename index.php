<?php

session_start();

if (isset($_POST['apikey']))
{
    $_SESSION['apikey'] = $_POST['apikey'];
    saveAccountsToSession();
    saveOrganisationsToSession();

    echo "<p style='color:green'>Api key saved</p>";
}

if (isset($_POST['account']))
{
    $_SESSION['activeaccount'] = $_POST['account'];
    echo "<p style='color:green'>Account saved</p>";
}

if (isset($_POST['organisation']))
{
    $_SESSION['activeorganisation'] = $_POST['organisation'];
    echo "<p style='color:green'>Organisation saved</p>";
}

function saveAccountsToSession()
{
    $_SESSION['accounts'] = getAccounts();
}

function saveOrganisationsToSession()
{
    $_SESSION['organisations'] = getOrganisations();
}

function getAccounts()
{
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, "https://api.spryngpayments.com/v1/account");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "X-APIKEY: ".$_SESSION['apikey']
    ));

    $rawResponse = curl_exec($ch);

    return json_decode($rawResponse);
}

function getOrganisations()
{
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, "https://api.spryngpayments.com/v1/organisation");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "X-APIKEY: ".$_SESSION['apikey']
    ));

    $rawResponse = curl_exec($ch);

    return json_decode($rawResponse);
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Spryng Payments Tester</title>

    <link rel="stylesheet" href="resources/css/bootstrap.min.css">
</head>
<body>
<ul>

    <h2>Api key</h2>

    <form action="/" method="POST">
        <label for="apikey">Key</label>
        <?php

        if (isset($_SESSION['apikey']))
        {
            echo '<input type="text" id="apikey" name="apikey" value="' . $_SESSION['apikey'] . '">';
        }
        else
        {
            echo '<input type="text" id="apikey" name="apikey">';
        }

        ?>
        <input type="submit" value="Save">
    </form>

    <?php

    if (isset($_SESSION['apikey']))
    {
        echo "<h2>Account</h2>";

        echo "<form action='/' method='POST'>";
        echo "<select name='account' id='account'>";

        foreach($_SESSION['accounts'] as $account)
        {
            if (isset($_SESSION['activeaccount']) && $account->_id == $_SESSION['activeaccount'])
            {
                echo "<option value='" . $account->_id . "' selected='selected'>" . $account->name ."</option>";
            }
            else
            {
                echo "<option value='" . $account->_id . "'>" . $account->name ."</option>";
            }
        }

        echo "</select>";
        echo "<input type='submit' value='Save'>";
        echo "</form>";
    }

    if (isset($_SESSION['apikey']))
    {
        echo "<h2>Organisation</h2>";

        echo "<form action='/' method='POST'>";
        echo "<select name='organisation' id='organisation'>";

        foreach($_SESSION['organisations'] as $organisation)
        {
            if (isset($_SESSION['activeorganisation']) && $organisation->_id == $_SESSION['activeorganisation'])
            {
                echo "<option value='" . $organisation->_id . "' selected='selected'>" . $organisation->name ."</option>";
            }
            else
            {
                echo "<option value='" . $organisation->_id . "'>" . $organisation->name ."</option>";
            }
        }

        echo "</select>";
        echo "<input type='submit' value='Save'>";
        echo "</form>";
    }
    ?>

    <li><a href="/tests/ideal.php">Test iDeal (EMS)</a></li>
    <li><a href="/tests/card.php">Test Card (Credorax)</a></li>
    <li><a href="/tests/sepa.php">Test SEPA (SlimPay)</a></li>
    <li><a href="/tests/klarna.php">Test Klarna</a></li>
    <li><a href="/tests/paypal.php">Test Paypal</a></li>
    <li><a href="/tests/sofort.php">Test SOFORT</a></li>
    <li><a href="/tests/3d.php">Test 3D Secure</a></li>
</ul>
</body>

<script src="resources/js/jquery.min.js"></script>
</html>
