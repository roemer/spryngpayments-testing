<?php

session_start();

if (!isset($_SESSION['apikey']) || !isset($_SESSION['activeaccount']))
{
    die('Please set api key and account on home page.');
}

const REDIRECT_URL_PREFIX   = "https://57b3135f.ngrok.io/redirect/ideal.php";

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

$_SESSION['dynamic_descriptor'] = generateRandomString(24);

$post = array(
    "account"               => $_SESSION['activeaccount'],
    "amount"                => $_POST['amount'],
    "customer_ip"           => $_SERVER['REMOTE_ADDR'],
    "dynamic_descriptor"    => $_SESSION['dynamic_descriptor'],
    "user_agent"            => $_SERVER['HTTP_USER_AGENT'],
    "details"               => [
        "redirect_url"  => REDIRECT_URL_PREFIX,
        "issuer"        => $_POST['issuer']
    ]
);

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, "https://api.spryngpayments.com/v1/transaction/ideal/initiate");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    "X-APIKEY: ".$_SESSION['apikey']
));

$rawResponse = curl_exec($ch);
$jsonResponse = json_decode($rawResponse);

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Redirecting you...</title>
</head>
<body>
<h3>You are being redirected to the payment page...</h3>
</body>
<form id="redirectForm"></form>
<script src="../resources/js/jquery.min.js"></script>
<script>
    $(document).ready(function()
    {
        var apporvalUrl = "<?php echo $jsonResponse->details->approval_url; ?>";

        var form = $('#redirectForm');
        form.attr('action', apporvalUrl);
        form.submit();
    });
</script>

</html>
