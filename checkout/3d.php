<?php

session_start();

if (!isset($_SESSION['apikey']) || !isset($_SESSION['activeaccount']) || !isset($_SESSION['activeorganisation']))
{
    die('Please set api key, account and organisation on home page.');
}

const REDIRECT_URL_PREFIX   = "http://payments.app/redirect/3d.php";

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

$_SESSION['xid'] = generateRandomString(24);

$post = array(
    "account"               => $_SESSION['activeaccount'],
    "amount"                => (int) $_POST['amount'],
    "xid"                   => $_SESSION['xid'],
    "card"                  => $_POST['card'],
    "description"           => "Test Transaction"
);

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, "https://sandbox.spryngpayments.com/v1/3d/enroll");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    "X-APIKEY: ".$_SESSION['apikey']
));

$rawResponse = curl_exec($ch);
$jsonResponse = json_decode($rawResponse);

var_dump($jsonResponse);
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Redirecting you...</title>
</head>
<body>
<h3>You are being redirected to the payment page...</h3>
</body>
<form id="redirectForm" method="POST" action="<?php echo $jsonResponse->url ?>">
    <label for="PaReq">PaReq</label>
    <input type="text" name="PaReq" id="PaReq" value="<?php echo $jsonResponse->pareq; ?>"><br>

    <label for="TermUrl">Term URL</label>
    <input type="text" name="TermUrl" id="TermUrl" value="<?php echo REDIRECT_URL_PREFIX; ?>"><br>

    <label for="MD">MD</label>
    <input type="text" name="MD" id="MD" value="<?php echo $_SESSION['xid'] ?>"><br>

    <input type="submit" value="Submit data to 3D Server">
</form>
<script src="../resources/js/jquery.min.js"></script>
<script>
    $(document).ready(function()
    {
        //$('#redirectForm').submit();
    });
</script>

</html>
